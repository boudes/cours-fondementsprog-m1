
### Question A

```ocaml
utop # 1 + 2;;
- : int = 3
```
```scala
@ 1 + 2 
res0: Int = 3
```

```js
> 1 + 2
3
```

```python
>>> 1 + 2
3

```

```clojure
user=> (+ 1 2)
3
```

### Question B

```ocaml
utop # (fun x -> x + 1);;
- : int -> int = <fun>

utop # (fun x -> x + 1)(2);;
- : int = 3
```
```scala
@ ((x:Int) => x + 1) 
res1: Int => Int = ammonite.$sess.cmd1$$$Lambda$1365/0x00000008407a6040@1bdaa23d

@ ((x:Int) => x + 1)(2) 
res3: Int = 3
```

```js
> (x => x + 1)
[Function (anonymous)]
> (x => x + 1)(2)
3

```

```python
>>> lambda x : x + 1
<function <lambda> at 0x7f3824c3d1b0>

>>> (lambda x : x + 1)(2)
3
```

```clojure
user=> (fn [x] (+ x 1))
#object[user$eval2007$fn__2008 0x4e1e7e90 "user$eval2007$fn__2008@4e1e7e90"]
user=> ((fn [x] (+ x 1)) 2)
3
```


### Question D

```ocaml
utop # List.map (fun x -> x * 10) [1;2;3];;
- : int list = [10; 20; 30]
```

```scala
@ List(1, 2, 3).map((x) => x * 10) 
res5: List[Int] = List(10, 20, 30)

@ List(1, 2, 3).map(_ * 10) 
res6: List[Int] = List(10, 20, 30)
```

```js
> [1,2,3].map(x => x * 10)
[ 10, 20, 30 ]

```

En python on obtient un itérateur (le calcul est suspendu) et non une liste. On l'exécute pour obtenir une liste.

```python
>>> map(lambda x: x * 10,[1, 2, 3])
<map object at 0x7f3824c2c460>

>>> list(map(lambda x: x * 10,[1, 2, 3]))
[10, 20, 30]
```

En clojure `map` fonctionne encore s'il y a plusieurs arguments / plusieurs listes.

```clojure
user=> (map (fn [x] (* x 10)) [1,2,3])
(10 20 30)
user=> (map (fn [x] (* x 10)) [1 2 3])
(10 20 30)
user=> (map (fn [x y] (+ (* x 10) y)) [1 2 3] [4 5 6])
(14 25 36)
```
