% -*- coding: utf-8 -*-
\chapter{Éléments de calculabilité}


Jusqu'à maintenant nous nous sommes intéressés à la manière de
résoudre des problèmes par un calcul (tri, organisation de données,
recherche etc.), en particulier à l'aune du temps d'exécution. Mais
tous les problèmes ne peuvent pas être résolus par un calcul. En
particulier, il existe des problèmes qui s'énoncent sur des données
finies et parfaitement informatisables dont on sait (on l'a démontré)
qu'ils ne sont pas solubles par un calcul. Ce type de résultats est du
domaine de la calculabilité. Ils supposent de se donner un modèle de
calcul simple et précis (souvent les machines de Turing), mais
l'exposé de ces modèles nécessiterait un cours complet. Nous faisons
donc un compromis et nous gardons comme modèle de calcul un langage de
programmation du type langage C et des programmes s'exécutants sur nos
ordinateurs.

Ce chapitre expose quelques uns des résultats les plus spectaculaires
de calculabilité.  Les modèles de calcul usuels en calculabilité
datent pour la plupart d'avant l'informatique des ordinateurs. Le
résultat d'indécidabilité de l'arrêt est due à Alan Turing, et date de
1936 (il s'agit d'une formulation plus calculatoire du célèbre
théorème d'incomplétude de Kurt Gödel).  Le théorème de Rice est quant
à lui daté de 1953.  Pour l'un et l'autre de ces résultats, il a fallu
beaucoup de temps pour que leurs formulations modernes s'imposent et
que leurs implications soient comprises dans la communauté
informatique.

Nous commençons par quelques rappels sur les notions de dénombrement
et de fonction (au sens mathématique).

%Définition moderne de la notion de fonction mathématique.  
\begin{definition}
  Une \emph{fonction} au sens mathématique est la donnée de deux
  ensembles, un ensemble d'entrées $E$, un ensemble de sorties $S$,
  et d'une relation $f\subset E\times S$, c'est à dire d'un ensemble
  de couples $(e,s)$, telle que la relation $f$ est déterministe,
  c'est à dire que si $(e,s)$ et $(e,s')$ sont tout deux dans la
  relation alors $s = s'$.
\end{definition}
Lorsque
$(e,s)$ est dans la relation $f$, on écrit $f:e\mapsto s$, ou encore
$f(e) = s$. Lorsque pour tout $e\in E$, $f(e)$ est défini (c'est à
dire qu'il existe un $s\in S$ tel que $f(e) = s$) la fonction est
\emph{totale}, autrement la fonction est \emph{partielle}. Il suffit
d'adjoindre un élément distingué $\bot$ à l'ensemble $S$ pour
représenter les fonctions partielles de $E\to S$ par les fonctions
totales de $E\to S\cup\{\bot\}$ (on envoie les $e$ pour lesquels $f$
n'est pas définie sur $\bot$).

\begin{definition}
  On dit d'un ensemble $E$ qu'il est \emph{dénombrable} lorsque il
  existe une bijection entre $E$ et $\bbbn$.
\end{definition}

Toute partie infinie de $\bbbn$ est dénombrable. On dit d'un ensemble
qu'il est au plus dénombrable lorsqu'il existe une bijection entre
$E$ et une partie de $\bbbn$.

%%%\hookrightarrow
%%%\rightharpoonup

\begin{proposition}[Cantor]
  L'ensemble $\mathcal{P}(\bbbn)$  des parties de $\bbbn$ est non-dénombrable. 
\end{proposition}

La preuve introduit le célèbre argument de la diagonale, de Cantor.
\begin{proof}
  Par contradiction. Si $\mathcal{P}(\bbbn)$ est dénombrable, il
  existe une énumération (bijective) : $e:\bbbn\to
  \mathcal{P}(\bbbn)$. On considère alors la partie de $\bbbn$
  suivante :
  \begin{gather*}
    D = \{k\in\bbbn \mid  k\notin e(k)\}
  \end{gather*}
  Cette partie $D$ doit avoir un numéro $n$ dans l'énumération $e(n) =
  D$. Est-ce que $n\in D$ ? Si oui, $n\notin e(n) = D$ ce qui n'est
  pas possible. Si non,  alors $n$ est tel que $n\notin e(n)$ donc
  $n\in D$ ce n'est pas possible non plus. Il y a contradiction dans
  les deux cas, c'est donc que l'énumération bijective n'existe pas
  c'est à dire que $\mathcal{P}(\bbbn)$ n'est pas dénombrable.
\end{proof}


\begin{proposition}
  L'ensemble $\bbbn^\bbbn$ des fonctions mathématiques de $\bbbn\to
  \bbbn$ est non-dénombrable. Ceci est également vrai pour l'ensemble
  $(\bbbn\cup\{\bot\})^\bbbn$ des fonctions partielles de
  $\bbbn\to\bbbn$.
\end{proposition}

\begin{proof}
  Il suffit de considérer le sous ensemble
  \begin{gather*}
    X = \{ \chi_A\mid A \subseteq \bbbn\}
  \end{gather*}
des fonctions $\chi_A:n\mapsto
\begin{cases}
  1\text{ si }n\in A\\
  0\text{ sinon}
\end{cases}$
qui testent l'appartenance à une partie $A$ de $\bbbn$. Cet ensemble à
autant d'éléments que $\mathcal{P}(\bbbn)$, il est donc non
dénombrable, et il s'en suit que $\bbbn^\bbbn$ ne peut pas être dénombrable.
\end{proof}


On peut toujours considérer qu'un programme calcule une fonction au
sens mathématique : il associe de manière déterministe une sortie d'un
certain ensemble $S$ à une entrée d'un certain ensemble $E$. Cette
fonction est en général partielle : le programme boucle sur certaines
entrée, et ne renvoie pas de résultat, la fonction n'est donc pas
définie sur ces entrées. Par opposition à partielle, une fonction qui à chaque
entrée associe une sortie est dîte totale. On peut totaliser la
fonction (la rendre totale) en ajoutant un symbole $\bot$ pour dénoter
que le programme boucle sur l'entré $e$.

\begin{definition}
  Une \emph{fonction calculable} est une fonction (au sens
  mathématique) telle qu'il existe au moins un programme la calculant.
\end{definition}
Pour une même fonction calculable il est possible d'écrire
une infinité de programmes qui la calcule (il suffit de partir de l'un
ces programmes et d'y ajouter des blocs d'instructions arbitraire
n'ayant pas d'incidence sur le résultat du calcul).  

On oppose parfois l'intension, (ou la compréhension) des programmes à
l'extension des fonctions. Intuitivement, une extension est une liste
exhaustive souvent infinie : la liste des couples $(e,s)$ mis en
relation par une fonction, la liste des entiers pairs, tandis que
l'intension est une représentation finie d'un ensemble potentiellement
infinie d'objets, par exemple un programme calculant $f(e)$ pour tout
$e$, ou l'écriture $\{n\in\bbbn\mid 2\texttt{ divise }
n\}$.


\begin{proposition}
  L'ensemble des mots finis d'entiers est dénombrable par une fonction
  calculable de réciproque calculable.  L'ensemble
  $\mathcal{P}_\text{fin.}(\bbbn)$ des parties finies de $\bbbn$ est
  dénombrable.
\end{proposition}

\begin{proof}
  Il est plus facile de démontrer que l'ensemble des mots finis
  non-vides d'entiers est dénombrable (voir plus bas). On ajoute le mot vide en
  utilisant un décalage $n\mapsto n+1$ dans l'énumération précédente. Pour ce qui
  de la deuxième partie de la proposition, on peut alors rapidement
  conclure par le théorème de Cantor-Bernstein mais c'est hors du
  programme de ce cours. Ce théorème très pratique établit que s'il
  existe une injection de $f:A\hookrightarrow B$ et une injection
  $g:B\hookrightarrow A$ alors il existe une bijection entre $A$ et
  $B$.

  Montrons que l'ensemble des mots finis non-vides d'entiers est dénombrable.
  Rappelons que $\bbbn\times\bbbn$ est dénombrable c'est à dire qu'il
  existe une bijection calculable dans les deux sens $f:\bbbn\to
  \bbbn\times\bbbn$. Pour les septiques, prendre l'inverse de la
  fonction $(p,q)\mapsto 2^p(2q+1)$ (notez que cet inverse est
  calculable).  À un entier $k$ on peut alors associer une paire
  $f(k)=(n,c)$ où $n + 1$ sera la longueur du mot
  recherché. Il suffit alors d'appeler $f$, $n$ fois sur le
  second terme de la paire trouvée à chaque étape, pour construire un
  mot d'entiers : si $n = 3$ on fait par exemple $f(c)=(a_1, c_1)$,
  $f(c_1)=(a_2,c_2)$, $f(c_2) = (a_3, c_3)$, $a_4 = c_3$, ce qui donne
  le mot $a_1a_2a_3a_4$. 
\end{proof}


Un \emph{problème de décision} sur un ensemble $E$ est un énoncé
$D(e)$ qui pour chaque élément $e$ de $E$ est soit
vrai soit faux. C'est donc une fonction $D:E\to
\{\text{vrai},\text{faux}\}$. Un problème de décision est trivial
lorsque il est vrai pour tous les éléments de $E$ (l'ensemble $\{e\in
E\mid D(e)\}$ est égal à $E$) ou bien lorsqu'il faux pour tous les
éléments de $E$ (l'ensemble $\{e\in E\mid D(e)\}$ est vide).

D'un point de vue algorithmique, un problème de décision est dit
\emph{décidable} lorsqu'il existe un un programme qui
le calcule ($D:E\to \{\text{vrai},\text{faux}\}$ est une fonction
calculable). Autrement il est dit indécidable. 

Attention, pour qu'un problème soit décidable il faut que le programme
qui le calcule termine pour chaque entrée $e\in E$ en donnant la
réponse exacte. Un problème indécidable $D$ est dit
\emph{semi-décidable} lorsqu'il existe un programme qui termine et
répond vrai sur chaque entrée $e\in E$ pour laquelle $D(e)$ est vrai,
et lorsqu'il termine sur les autres entrées, répond faux. Remarquez
qu'il existe nécessairement une entrée $e$, telle que $D(e)$ est faux,
pour laquelle le programme ne termine pas (autrement le problème
serait décidable). D'un point de vue calculatoire, la
semi-décidabilité n'a que peu d'intérêt.

\begin{proposition}
  Pour un langage donné, l'ensemble des programmes qui calculent une
  fonction partielle de $\bbbn\to\bbbn$ est dénombrable, et donc
  l'ensemble des fonctions calculables est dénombrable.
\end{proposition}

Les deux dernières propositions montrent qu'il y a beaucoup plus de
fonctions non-calculables que de fonctions calculables. 

\begin{lemme}
\label{lemme:enumprog}
Pour un langage donné, on peut énumérer l'ensemble des programmes, et
cette énumération peut être choisie calculable et de réciproque calculable.
\end{lemme}

\begin{theo}[Indécidabilité de l'arrêt]
 Il n'existe pas de programme $H$ prenant en argument n'importe quel
 programme $p$ et des arguments pour $p$, et qui déterminerait avec
 certitude si l'exécution de $p$ ses arguments s'arrêtera ou bouclera indéfiniment. 
\end{theo}


\begin{proof}
  On se donne une énumération des programme de $\bbbn\to\bbbn$. 

  On suppose que l'on peut programmer une fonction $h(p,k)$ qui teste
  l'arrêt du $p$-ième programme sur l'argument entier $k$ : si le
  $p$-ième programme sur l'entrée $k$ termine alors $h(p,k) = 1$ sinon
  $h(p,k) = 0$.  Par l'argument de la diagonale, on arrive à une
  contradiction. Celle-ci est simplement construite en écrivant le
  programme $g$ qui prend en entrée un entier $p$, calcule $h(p,p)$ et
  si le résultat est $1$ boucle éternellement, sinon renvoie $0$.  Par
  construction, $g$
  est un programme de $\bbbn\to\bbbn$. Donc $g$ a un numéro, disons
  $n$ dans notre énumération.  Est-ce que $g(n)$ termine ? Si oui
  c'est que $h(n,n) = 0$, mais alors le $n$-ième programme, $g$,
  boucle sur l'entrée $n$, ce qui est impossible. Si non c'est que
  $h(n,n) = 1$ mais alors le $n$-ième programme $g$, termine sur
  l'entrée $n$, ce qui est également impossible. C'est impossible dans
  les deux cas. Contradiction. Donc $h$ n'existe pas.

  Jusque là nous avons simplement utilisé le fait que nous pouvions
  associer un numéro unique à chaque programme, par contre rien
  n'exige que la numérotation soit surjective ou calculable ou de
  réciproque calculable. 

  Pour conclure à l'impossibilité de l'existence de $H$, il nous faut
  montrer que l'existence de $H$ implique celle de $h$.

  Si le programme $H$ existe il fonctionne sur les programmes de
  $\bbbn\to\bbbn$. Or d'après le lemme~\ref{lemme:enumprog}, il existe
  une énumération de ces programmes dont la réciproque est
  calculable. On a donc une fonction $e$ qui à chaque entier associe un
  programme $e(p)$ de type $\bbbn\to\bbbn$. On peut donc écrire le
  programme $h(p,k)$ comme étant le programme qui renvoie la valeur de
  $H(e(p),k)$. Mais puisque $h$ n'existe pas c'est que $H$ non plus.
\end{proof}

\begin{proposition}[Indécidabilité de l'arrêt sur un argument (temps)]
  Parmi les algorithmes calculant une fonction partielle de
  $\bbbn\to\bbbn$ savoir lesquels terminent sur l'argument zéro est indécidable.
\end{proposition}

\begin{rem}
  Si les algorithmes sont exécutés sur une machine à mémoire finie,
  alors le nombre d'états de n'importe quel programme est fini. Dans
  ce cas il est théoriquement possible de décider de l'arrêt de
  n'importe quel programme s'exécutant sur cette machine. En effet,
  sur une machine à $N$ états si un programme change plus de $N$ fois
  d'état avant l'arrêt du programme, alors le programme ne s'arrêtera
  jamais (il passe par une suite d'états cycliques). Nos ordinateurs
  sont à états finis mais attention $N$ est en général tellement grand
  que cet argument de finitude est inapplicable en pratique. Pensez
  aux $2^{8\,589\,934\,592}$ états d'un ordinateur ayant un Gibi
  octet de mémoire (sans parler des mémoires de masse) !
\end{rem}

Un propriété des algorithmes ou des programmes qui s'énonce sur les
fonctions calculées est dîte \emph{extensionnelle}. La notion est assez
souple car on peut faire varier ce qui est considéré comme une entrée
et ce qui est considéré comme une sortie d'un programme. Par exemple,
le fait d'afficher <<bonjour>> est une propriété extensionnelle des
programmes, pour la notion de sortie qui inclue les sorties écran.

\begin{theo}[Théorème de Rice]
  Toute propriété extensionnelle sur les programmes est soit triviale
  soit indécidable.
\end{theo}

\begin{proof}
  On se ramène à l'indécidabilité de l'arrêt.
\end{proof}


\begin{proposition}[Indécidabilité de l'égalité extensionnelle
  (espace)]
\label{prop:riceespace}
  Si on restreint notre champs d'étude aux algorithmes de type
  $\bbbn\to\bbbn$ qui terminent toujours (sur chaque argument), savoir si
  deux de ces algorithmes calculent la même fonction mathématique est
  un problème indécidable (il n'existe pas de programme scrutateur qui donne la
  bonne réponse à tous les coups).
\end{proposition}

\begin{proof}
  On simule facilement l'exécution d'un programme $p$ sur une entrée
  particulière, qui ne termine pas forcément, à l'aide d'un algorithme
  $\bbbn\to\bbbn$ qui terminent tout le temps. Soit $f_p$ l'algorithme
  qui prend en entré $n$ et lance l'exécution de $p(0)$ pendant $n$
  cycles d'horloge. Si $p(0)$ a terminé en $n$ cycles ou moins il
  renvoie $1$, et sinon il renvoie $0$. On forme ainsi un ensemble de
  programmes $f_p$ qui terminent toujours. Supposons que nous sachions
  toujours décider le problème de la proposition. Alors nous serions
  capables de savoir si la fonction calculée par un $f_p$ prend la
  valeur $1$ au bout d'un certain temps, ce qui reviendrait à pouvoir
  décider si le programme $p$ termine sur l'entrée $0$. Contradiction.
\end{proof}

\begin{rem}
  Le problème de l'arrêt est semi-décidable.
\end{rem}

\begin{rem}
  Si $E$ est un ensemble fini de données disons une partie finie de
  $\bbbn$, et que l'on considère uniquement les algorithmes
  $E\to\bbbn$ qui terminent toujours, alors il est facile de tester si
  deux de ces algorithmes calculent la même fonction de $E\to\bbbn$
  (en calculant effectivement sur toutes les valeurs de $E$).
\end{rem}

Quelques conséquences du théorème de Rice :
\begin{itemize}
\item Il est impossible de déterminer automatiquement, pour tous les
  programmes, si un programme va générer une erreur et planter.
\item Savoir détecter tous les programmes qui contiennent un virus est
  un problème indécidable (l'anti-virus parfait n'existe pas).
\item Si on donne par défaut des droits en écriture sur un compte
  utilisateur à un langage de script de pages web, alors il est
  impossible qu'un programme (le navigateur, un pare-feu etc.)
  puisse toujours détecter si un script risque d'utiliser ce droit de manière
  dangereuse ou malveillante.
\item Il est impossible d'écrire un ramasse-miette qui évite à tous
  les coups les fuites de mémoire.
\item L'analyse syntaxique des programmes Perl 5 est indécidable (plus
  précisément, la question indécidable est de savoir si un programme
  Perl 5 correspond à un arbre syntaxique donné).
\end{itemize}

On peut étendre facilement le théorème de Rice et la
proposition~\ref{prop:riceespace} de manière à prendre en compte le
temps de calcul.  Il est par exemple indécidable de déterminer si deux
programmes qui terminent et ont asymptotiquement le même temps
d'exécution en pire cas calculent la même fonction (Asperti 2006). Par
exemple, parmi les programmes qui s'exécutent en temps $\Theta(N\log
N)$, il est impossible de savoir à coup sûr lesquels font un tri sur
leur entrée.

En terme de complexité en temps ou en espace des programmes, on
définit des classes de programmes, qui ont l'avantage d'être closes
par composition\footnote{Ces classes sont très connues. D'ailleurs
  vous avez peut être déjà entendu parler de la grande question à un
  million de dollars $\textsf{P}\neq \textsf{NP}$ ?}. Par exemple la
classe \textsf{P} des programmes qui calculent en temps polynomial est
l'ensemble des programmes $f$ pour lesquels il existe une polynôme
réel $Q(X)$ tel que sur une donnée $d$, $f$ calcule en pire cas en
temps $O(Q(N))$ où $N$ est la taille de la donnée $d$. Si deux
programmes $f$ et $g$ sont dans \textsf{P}, alors le programme qui
calcule $g(f(d))$ pour toute donnée $d$ est également dans \textsf{P}.

\begin{theo}[Folklore, Terui]
  Savoir si un programme qui termine toujours calcule en temps
  polynomial est indécidable.
\end{theo}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "cours"
%%% End:

