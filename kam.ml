type exp = Var of string
         | Lambda of string * exp
         | App of exp * exp;;

(* examples *)
let e1 = Lambda ("x", Lambda ("y", App(Var("x"), Var("y"))));;
let e2 = Lambda ("x", Var "y");;
let e3 = App(Lambda ("z", Var "z"), Var "y");;
let e4 = App(e2,e3);;

type env = EmptyDontUse | Env of (string * closure) list 
and closure = exp * env;;

type stackelement = A of closure | F of closure | NA of closure;;
type stack = stackelement list;;

type state = Stopped | State of (exp * env * stack);;
type rulename = Nothing | Push | Pop | Deref | Swap;;

(* pretty printing *)
let rec pp e =
  let rec ppp e = match e with
  | Var(x) -> x
  | App(e1,e2) -> "("^(ppp e1)^" "^(ppp e2)^")"
  | Lambda(x,e) -> "(\\lambda "^x^"."^(pp e)^")"
  in
  match e with
  | Var(x) -> x
  | App(e1,e2) -> "("^(ppp e1)^" "^(ppp e2)^")"
  | Lambda(x,e) -> "\\lambda "^x^"."^(pp e);;

pp e1;;                            
pp e4;;

let rec ppenv e = match e with
  | Env []
  | EmptyDontUse -> "\\emptyset"
  | Env ((s,c)::(s2,c2)::es) -> s^"\\to"^(ppclosure c)^", "^(ppenv (Env ((s2,c2)::es)))
  | Env ((s,c)::[]) -> s^"\\to"^(ppclosure c)
and ppclosure (t, e) = "("^(pp t)^","^(ppenv e)^")";;

let ppstackel el =  match el with
  | A(c) -> "A"^(ppclosure c)
  | F(c) -> "F"^(ppclosure c)
  | NA(c) -> ppclosure c;;

let ppstack cs = match cs with
  | [] -> "\\varepsilon"
  | el::cs -> List.fold_left (fun a -> fun b ->  a^"::"^(ppstackel b)) (ppstackel el)  cs;;

let ppstate s = 
  match s with
  | (Nothing, State (t, e, p)) ->
    print_endline ("\\AXC{$"^(pp t)^"\\quad"^(ppenv e)^"\\quad"^(ppstack p)^"$}")
  | (Push, State (t, e, p)) ->
    print_endline ("\\kpush{"^(pp t)^"}{"^(ppenv e)^"}{"^(ppstack p)^"}")
  | (Pop, State (t, e, p)) ->
    print_endline ("\\kpop{"^(pp t)^"}{"^(ppenv e)^"}{"^(ppstack p)^"}")
  | (Deref, State (t, e, p)) ->
    print_endline ("\\kderef{"^(pp t)^"}{"^(ppenv e)^"}{"^(ppstack p)^"}")
  | (Swap, State (t, e, p)) ->
    print_endline ("\\kswap{"^(pp t)^"}{"^(ppenv e)^"}{"^(ppstack p)^"}")
  | (_, Stopped) -> print_endline "\\DP";;


(* execution *)
let rec step_bn s = match s with
  | State(t, EmptyDontUse, p) -> step_bn (State(t, Env [], p)) (* DontUse *)
  | State(App(t,u), e, p) -> (Push, State(t, e, (NA(u, e))::p))
  | State (Lambda(x,t), Env e, (NA(c))::p) -> (Pop, State(t, Env ((x,c)::e), p))
  | State(Var(x), Env e1, p) -> (match List.assoc_opt x e1 with
    | Some (t,e2) -> (Deref, State (t, e2, p))
    | None -> (Nothing, Stopped))
  | _ -> (Nothing, Stopped);;


let rec step_bv s = match s with
  | State(t, EmptyDontUse, p) -> step_bv (State(t, Env [], p)) (* DontUse *)
  | State(App(t,u), e, p) -> (Push, State(t, e, (A(u, e))::p))
  | State (Lambda(x,t), e, (A((u,e2)))::p) -> (Swap, State(u, e2, F((Lambda(x,t), e))::p))
  | State(Var(x), Env e, p) -> (match List.assoc_opt x e with
    | Some (t,e2) -> (Deref, State (t, e2, p))
    | None -> match p with
      | (F((Lambda(x2,t2), Env e2)))::p2
        -> (Pop, State(t2, Env ((x2,(Var(x),Env e))::e2), p2))
      | _ -> (Nothing, Stopped)
    )
  | State (Lambda(x,t), e, (  F( (Lambda(x2,t2), Env e2) )  )::p)
    -> (Pop, State(t2, Env ((x2,(Lambda(x,t),e))::e2), p))
  | _ -> (Nothing, Stopped);;


let init t = State(t, Env [], []);;
let eval step s  =
  let rec eval s trace  = match step s with
    | (_, Stopped) -> trace
    | (r,s2) ->  eval s2 ((r, s2)::trace) in
  List.rev (eval s []);;

(* execution + LaTeX pretty printing *)
let rec texeval step t =
  let s = init t in
  let states = (Nothing, s)::(eval step s) in
  List.iter ppstate states;;

let export e1 =  
  (print_newline ();
   print_endline("Évaluation de $"^pp(e1)^"$ par nom :");
   print_endline("#+begin_export latex");
   print_endline("\\begin{gather*}");
   texeval step_bn e1;
   print_endline("\\DP");
   print_endline("\\end{gather*}");
   print_endline("#+end_export");
   print_newline ();
   print_endline("Évaluation de $"^pp(e1)^"$ par valeur :");
   print_endline("#+begin_export latex");
   print_endline("\\begin{gather*}");
   texeval step_bv e1;
   print_endline("\\DP");
   print_endline("\\end{gather*}");
   print_endline("#+end_export");
  );;

(* Example *)
let () = (export (App(Lambda ("x",Var "x"),Var "y"));
          export (App(Lambda("z", Var "z"), App(Lambda("x", Var "x"), Lambda("y", Var "y")))));;

