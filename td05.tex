% Created 2021-11-09 mar. 22:59
% Intended LaTeX compiler: pdflatex
\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[margin=2cm]{geometry}
\usepackage[table]{xcolor}
\usepackage{xspace}
\usepackage{multicol}
\usepackage{bussproofs}
\usepackage{stmaryrd}
\usepackage{tikz}\usetikzlibrary{arrows,shapes,trees}
\renewcommand{\maketitle}{{\bigskip{\begin{center}\Large\textbf{Fondements de la programmation}\\[0.1cm] Exercices 05\end{center}}}}
\usepackage{fancyhdr}
\usepackage[french]{babel}
\author{P. Boudes}
\date{\today}
\title{Fondements de la programmation TD 05}
\hypersetup{
 pdfauthor={P. Boudes},
 pdftitle={Fondements de la programmation TD 05},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.3)}, 
 pdflang={French}}
\begin{document}

\maketitle
\pagestyle{fancyplain}
\fancyhf{}
\lhead{ \fancyplain{}{\raisebox{-1ex}{\includegraphics[scale=0.10]{./img/logoLipnNoir.pdf}} P. Boudes, P. Jacobé de Naurois}}
\rhead{ \fancyplain{}{M1 informatique 2021-2022}}
\rfoot{ \fancyplain{}{\thepage}}
%\rfoot{ }
\newcounter{questioncount}
\setcounter{questioncount}{0}
\newcommand{\question}[1][]{\addtocounter{questioncount}{1}\paragraph{Question \Alph{questioncount}. #1}}
\renewcommand{\subsection}[1]{\question[#1.]}

\overfullrule=5pt


\newcommand{\bbbn}{\mathbb{N}}

\EnableBpAbbreviations
\newcommand{\num}{\operatorname{num}}
\newcommand{\str}{\operatorname{str}}
\newcommand{\valeur}{\operatorname{val}}
\newcommand{\letname}{\operatorname{let}}
\newcommand{\plusname}{\operatorname{plus}}
\newcommand{\prodname}{\operatorname{prod}}
\newcommand{\sumname}{\operatorname{sum}}
\newcommand{\fixname}{\operatorname{fix}}
\newcommand{\letsurface}[3]{let #1 = #2 in #3}
\newcommand{\letformal}[3]{\letname(#2, #1.#3)}
\newcommand{\apply}{\operatorname{apply}}
\newcommand{\applyformal}[2]{\apply \{#1\}(#2)}
\newcommand{\applysurface}[2]{#1(#2)}
\newcommand{\apformal}[2]{\operatorname{ap}(#1;#2)}
\newcommand{\apsurface}[2]{#1(#2)}
\newcommand{\abssurface}[3]{\ensuremath{\lambda} (#1:#2) #3}
\newcommand{\absformal}[3]{\ensuremath{\lambda}\{#2\}(#1.#3)}
\newcommand{\funname}{\operatorname{fun}}
\newcommand{\funformal}[6]{\funname \{#1; #2\}(#3.#4;#5.#6)}
\newcommand{\funsurface}[6]{fun #5(#3: #1): #2 = #4 in #6}
\newcommand{\error}{\operatorname{error}}
\newcommand{\err}{\operatorname{err}}
\newcommand{\funsub}[3]{\ensuremath{\llbracket#1.#2/#3\rrbracket}}



\section{Lambda-calcul pur et simplement typé}
\label{sec:orge1ba2d7}

\begin{multicols}{2}

On peut ajouter au langage EF un ensemble (éventuellement infini) de symboles de types \(A, B,C, \ldots\), que nous appelons types atomiques, de même que nous utilisons depuis le départ un ensemble infini de symboles parmi les expressions pour représenter les variables. Vous pouvez penser à ces symboles comme des variables de types ou de nouvelles constantes parmi les types. Le lambda-calcul simplement typé s'obtient alors à partir du langage EF et en gardant que l'application et l'abstraction dans les expressions, et les règles de typages associées.

\subsection{Typage}
\label{sec:org64195c9}
Donner une expression de type \(A\to (A\to B)\to B\). Est-ce que \(\lambda (x: \tau) (x\; x)\) est typable pour un certain type \(\tau\) ?

On peut voir le lambda-calcul pur comme l'ensemble des expressions que l'on peut former avec l'abstraction sans annotation de type (juste \(\lambda x. e\) au lien de \(\lambda (x: \tau) e\)) et l'application, sans contrainte de typage. Une expression du lambda-calcul pur peut donc être typable en lambda-calcul simplement typé ou ne pas l'être. Une manière alternative de voir le lambda-calcul pur est de considérer qu'il n'y a qu'un seul type \(o\) et qu'il satisfait une égalité \(o\to o = o\). Toutes les expressions du lambda-calcul deviennent alors typables de type \(o\).

Un point important mais que nous ne développerons pas ici est que la dynamique du lambda-calcul n'est en général pas limitée à l'évaluation par valeur ou l'évaluation par nom.

\subsection{Opérateur de point-fixe}
\label{sec:org66eb325}
En lambda-calcul pur on peut écrire des expressions dont la dynamique est celle de \texttt{fix}. Le plus connu est l'opérateur de point fixe de Church \[Y = \lambda f.(\lambda x.(f\; (x\; x))) (\lambda x.(f\; (x\; x))).\]
\begin{enumerate}
\item Montrer que \(Y\) n'est pas simplement typable mais qu'en utilisant un type \(o = o\to o\) il le devient.
\item On a \(Y = \lambda f. e\) pour une certaine expression \(e\), donc \((Y\; f) \mapsto e\). Montrer que \(e\mapsto (f\; e)\). Comment \(Y\) se compare t'il à \texttt{fix} ?
\end{enumerate}

\paragraph{Calculabilité.} Le lambda-calcul simplement typé a un pouvoir calculatoire limité. Son expressivité au sens Church-Turing, c'est à dire l'ensemble des fonctions \(\bbbn^k\to\bbbn\) qu'il permet de calculer correspond aux polynômes (étendus avec des conditionnelles par exemple des \texttt{if then else}). Au contraire le lambda-calcul pur est Turing complet (c'est même le Church de Church-Turing), raison pour laquelle certains calculs ne terminent pas.

\subsection{Instanciation}
\label{sec:orga3ee24e}
Trouver trois types que l'on puisse substituer aux variables \(X\), \(Y\) et \(Z\) de façon à  rendre \(e\) typable dans EF. 
 \[ e = \lambda (x: X) \lambda (y: Y) \lambda (z: Z)\; ((x \; z)\; (y\; z)) \] 
\end{multicols}

\section{Polymorphisme(s)}
\label{sec:orge5f2a6a}


\begin{quote}
« Abstraction principle: Each significant piece of functionality in a
program should be implemented in just one place in the source code.
Where similar functions are carried out by distinct pieces of code, it
is generally beneficial to combine them into one by abstracting out the
varying parts. » Benjamin C. Pierce
\end{quote}

\begin{multicols}{2}

\noindent Les systèmes de types qui permettent de réutiliser une même expression avec différents types sont dits polymorphiques. On en distingue plusieurs.

Le \textbf{polymorphisme ad-hoc}, correspond à la surcharge d'opérateurs. On définit plusieurs fonctions ayant le même nom et des signatures différentes, ce sont les types des arguments qui détermineront (durant une analyse statique ou à l'exécution) laquelle appliquer. Il s'applique aussi dans des cas de sous-typage, ou d'introspection de types par exemple dans un filtrage de motifs par types. 

Le \textbf{polymorphisme paramétrique} (ou générique) consiste en utiliser des variables de types qui seront instanciées par des types particuliers lorsque nécessaire. Les définitions paramétriques sont uniformes au sens où toutes leurs instances ont le même comportement.

Le polymorphisme de première classe, comme celui du système F (voir plus bas), est le plus puissant mais la typabilité d'une expression y est indécidable. Il est plus courant d'utiliser un let-polymorphisme qui n'autorise le polymorphisme que pour les définitions \emph{top-level}.

Dans le système F, les deux fonctions suivantes (écrites en OCaml) sont typables tandis que que dans OCaml qui utilise le let-polymorphisme la seconde n'est pas typable.

\begin{verbatim}
fun x -> x;;
- : 'a -> 'a = <fun>
fun f -> float_of_int (f 3) +. f 3;;  
Line 1, characters 31-34:
Error: This expression has type int but
an expression was expected of type float
\end{verbatim}


Le let-polymorphisme est celui que l'on rencontre dans les langages tels que ML, SML, OCaml, Haskell. L'algorithme d'Hindley-Milner, que nous ne détaillerons pas permet de retrouver à coup sûr le type \emph{principal} (au sens de plus général) d'une expression lorsqu'elle est typable. L'expression \(\lambda x. x\) peut être typée \(\num\to\num\) ou \(\str\to\str\) mais son type principal sera \(\forall A. A\to A\) où \(A\) est une variable de type. On a une relation d'ordre partiel \(\sqsubseteq\) entre types qui repose sur la substitution de variables : \(\forall A. A\to A \sqsubseteq \num\to\num\) car on peut substituer \(A\) par \(\num\)  et également \(\forall A. A\to A \sqsubseteq \forall B. (B\to B)\to (B\to B)\) car on peut substituer \(A\) par \((B\to B)\).

Le polymorphisme paramétrique est si générique qu'il implique l'existence de ce que Philip Wadler appelle des \emph{théorèmes gratuits}. Ceux-ci permettent, à partir d'un type, de déduire une partie de l'implémentation d'une fonction. Par exemple, lorsqu'on dispose du constructeur \texttt{List[A]} pour les listes d 'expressions d'un même type générique \texttt{A}, alors une fonction de type \(\forall A\) \texttt{List[A] -> num} (\texttt{'a list -> int} en OCaml), ne dépendra que du nombre d'éléments dans la liste passée en argument. Ou encore une fonction de type \(\forall A,B,C. (A\to B)\to (B\to C) \to A \to C\) doit nécessairement être la composition. Le polymorphisme générique conduit ainsi à une forme de développement appelée \emph{type-driven developpment} développement conduit par les types, où l'on commence par exprimer le type d'une fonction générique avant de réfléchir à son implémentation qui bien souvent découle directement de ce type et des types des expressions disponibles dans le contexte. 

Les algorithmes de typage des systèmes let-polymorphiques sont assez performants mais leurs temps d'exécution peuvent être exponentiels en la taille de leurs entrées comme l'ont montré Harry Mairson ou Pavel Urzyczyn. Harry Mairson donne cet exemple dont le typage prend \emph{très} longtemps :
\begin{verbatim}
let f0 = fun x -> (x, x) in
let f1 = fun y -> f0 (f0 y) in
let f2 = fun y -> f1 (f1 y) in
let f3 = fun y -> f2 (f2 y) in
let f4 = fun y -> f3 (f3 y) in
let f5 = fun y -> f4 (f4 y) in
f5 (fun z -> z)
\end{verbatim}

Le système F consiste en le lambda-calcul simplement typé auquel on rajoute la quantification universelle sur les types \(\tau = A \mid \tau \to \tau \mid \forall A. \tau\), pour les expressions l'abstraction sur les types \(\Lambda A. t\) l'application d'une expression à un type \((e\;\tau)\). Pour le typage on ajoute aux contextes les variables de types \(\Gamma = \emptyset \mid x:\tau \mid A\) et les deux règles :
\begin{gather*}
\AXC{$\Gamma, A\vdash e:\tau$}\RL{T-abs}
\UIC{$\Gamma \vdash \Lambda A. e:\forall A. \tau$}
\DP
\end{gather*}
\begin{gather*}
\AXC{$\Gamma \vdash e: \forall A. \tau_1$}\RL{T-ap}
\UIC{$\Gamma \vdash (e\; \tau_2): [\tau_2/A] \tau_1$}
\DP
\end{gather*}

On étend la dynamique 
\begin{gather*}
\AXC{$e_1\mapsto e'_1$}\RL{}
\UIC{$(e_1\;\tau)\mapsto (e'_1\;\tau)$}
\DP\quad
\AXC{}\RL{}
\UIC{$((\Lambda A. e)\;\tau)\mapsto [\tau/A]e$}
\DP
\end{gather*}
et on convient qu'une expression \(\Lambda A. e\) est une valeur. Ce système peut être utilisé en conjonction avec EF pour en former une extension polymorphique que nous noterons EF2.
\end{multicols}

\subsection{Typer}
\label{sec:orgbea14f6}
\begin{enumerate}
\item L'identité polymorphe \texttt{id} \(\Lambda A \lambda (x: A) x\)
\item \texttt{double} \(\Lambda A.\lambda (f: A\to A) \lambda (a:A) (f\; (f\; a))\)
\item \texttt{selfApp} \(\lambda (x:\forall A. A\to A) ((x\; \forall A. A\to A)\; x)\)
\item \texttt{quadruple} \(\Lambda A. ((\texttt{double}\; [A\to A])\; (\texttt{double}\; A))\)
\end{enumerate}
\end{document}
