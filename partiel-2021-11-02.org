#+TITLE:   Fondements de la programmation partiel 1
#+AUTHOR:   P. Boudes
#+EMAIL:    boudes@univ-paris13.fr

#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE:  fr
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc toc:nil
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+LINK_UP:
#+LINK_HOME:
#+XSLT:
#+STARTUP: latex nolatexpreview entitiesplain
#+LaTeX_CLASS: article
#+LATEX_CLASS_OPTIONS: [11pt, a4paper]
#+LATEX_HEADER: \usepackage[margin=2.6cm]{geometry}
#+LATEX_HEADER: \usepackage[table]{xcolor}
#+LATEX_HEADER: \usepackage{xspace}
#+LATEX_HEADER: \usepackage{multicol}
#+LATEX_HEADER: \usepackage{bussproofs}
#+LATEX_HEADER: \usepackage{stmaryrd}
#+LATEX_HEADER: \usepackage{tikz}\usetikzlibrary{arrows,shapes,trees}
#+LATEX_HEADER: \renewcommand{\maketitle}{{\bigskip{\begin{center}\Large\textbf{Fondements de la programmation}\\[0.1cm] Partiel 1 (2h)\end{center}}}\smallskip}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage{calc}
#+LATEX_HEADER: \usepackage[french]{babel}
#+EXCLUDE_TAGS: noexport correction

#+BEGIN_EXPORT latex
\pagestyle{fancyplain}
\fancyhf{}
\lhead{ \fancyplain{}{\raisebox{-1ex}{\includegraphics[scale=0.10]{./img/logoLipnNoir.pdf}} P. Boudes, P. Jacobé de Naurois}}
\rhead{ \fancyplain{}{M1 informatique 2021-2022 \raisebox{-1ex}{\includegraphics[scale=0.10]{./img/grasshopper.jpg}}}}
\rfoot{ \fancyplain{}{%\thepage
}}
%\rfoot{ }
\newcounter{questioncount}
\setcounter{questioncount}{0}
\newcommand{\question}[1][]{\addtocounter{questioncount}{1}\paragraph{Question \Alph{questioncount}. #1}}
\renewcommand{\section}[1]{\question[#1.]}
#+END_EXPORT

#+begin_export latex
\overfullrule=5pt
#+end_export


#+begin_export latex
\EnableBpAbbreviations
\input{clockedbareme}
\BaremeSurDeuxH\DebutBareme
\newcommand{\num}{\operatorname{num}}
\newcommand{\str}{\operatorname{str}}
\newcommand{\valeur}{\operatorname{val}}
\newcommand{\letname}{\operatorname{let}}
\newcommand{\plusname}{\operatorname{plus}}
\newcommand{\prodname}{\operatorname{prod}}
\newcommand{\sumname}{\operatorname{sum}}
\newcommand{\fixname}{\operatorname{fix}}
\newcommand{\letsurface}[3]{let #1 = #2 in #3}
\newcommand{\letformal}[3]{\letname(#2, #1.#3)}
\newcommand{\apply}{\operatorname{apply}}
\newcommand{\applyformal}[2]{\apply \{#1\}(#2)}
\newcommand{\applysurface}[2]{#1(#2)}
\newcommand{\apformal}[2]{\operatorname{ap}(#1;#2)}
\newcommand{\apsurface}[2]{#1(#2)}
\newcommand{\abssurface}[3]{fun (#1:#2) => #3}
\newcommand{\absformal}[3]{\ensuremath{\lambda}\{#2\}(#1.#3)}
\newcommand{\funname}{\operatorname{fun}}
\newcommand{\funformal}[6]{\funname \{#1; #2\}(#3.#4;#5.#6)}
\newcommand{\funsurface}[6]{fun #5(#3: #1): #2 = #4 in #6}
\newcommand{\error}{\operatorname{error}}
\newcommand{\err}{\operatorname{err}}
\newcommand{\funsub}[3]{\ensuremath{\llbracket#1.#2/#3\rrbracket}}
#+end_export


/Document autorisé : la feuille langage EF fournie./


* Inférences de types 
Parmi les expressions suivantes, lesquelles sont typables et avec quel type et lesquelles ne sont pas typables et pourquoi ? \bareme{10}

1. =1 + "2"= \hfill $\plusname(\num[1],\str[2])$
2. \texttt{if 3 >= 0 then "hi" else fun (x:num) => x} \hfill $\operatorname{ifpos}(3,\str[\text{hi}],\absformal{x}{\num}{x})$
3. =let x = (22, 23) in Some(x) as {Some: num | None: Unit}=
4. =fun (f: str -> str) => fun (g: str -> num) => fun (x:str) => g(f(x))=
5. =(fun (x: num -> num) => x)(fun (x: num) => x)=

* Filtrage de motifs 
Dans les langages de programmation proposant le /pattern matching/ (le filtrage de motifs) le compilateur nous prévient lorsque nous oublions un cas. Le compilateur teste que notre filtrage est bien exhaustif et nous prévient par un avertissment lorsqu'il ne l'est pas. Par exemple, soit le code OCaml suivant : \bareme{4}
#+begin_src ocaml
type direction = Nord | Sud | Est | Ouest ;;
fun x -> match x with 
  | Est -> (-1, 0)
  | Ouest -> (1, 0)
  | Sud -> (0, -1) ;;
#+end_src

Ce code définit un type (que nous noterions ={Nord: Unit | Sud: Unit | Est: Unit | Ouest: Unit}= dans EF) et applique une fonction à un argument de ce type en oubliant un cas.
Le compilateur OCaml nous informe que nous avons oublié le cas =Nord=.
# Avec ce code nous aurons l'avertissement suivant à la compilation :
#+begin_src txt
Warning 8: this pattern-matching is not exhaustive.
Here is an example of a case that is not matched:
Nord
#+end_src

En Python 3.10 (sorti le 4 octobre 2021), qui introduit une syntaxe pour le filtrage de motifs =match case= l'exhaustivité des cas n'est par contre pas testée. Selon vous qu'en est-il dans le langage EF ? 

Créez une fonction qui prend en entrée un type somme et fait un filtrage de motifs en oubliant un cas (=Nord= par exemple). La fonction est-elle typable ? Conclure.

# * Dynamique
* Map 
Soit =T= le type ={Some: num | None: Unit}= réduire à une valeur ou une erreur l'expression suivante en justifiant chaque étape de réduction :
\bareme{4}

#+begin_src ascii
( (fun (f: num -> num) => fun (x: T) => match x with
    | Some(x1) => Some(f(x1)) as T
    | None(x2) => None(unit) as T ) (fun (y:num) => y + 1)) (None(unit) as T)
#+end_src

# N'hésitez pas à donner des noms aux sous-expressions.


* Langage 
Que signifie pour un langage de programmation que les fonctions y sont des citoyennes de première classe ? Donner un exemple de langage dans lesquels les fonctions ne sont pas des citoyennes de première classe et un exemple de langage dans lequel les fonctions sont des citoyennes de première classe. \bareme{2}

* Démonstration (bonus)
Prouver que si un terme $e$ est une erreur, alors il contient soit un sous-terme $\operatorname{div}(\num[n], \num[0])$ soit un sous-terme $\error$.

* Fonctions (suite) :noexport:
\paragraph{Lemme 8.5 (formes canoniques).} Si $e:\tau_1\to\tau_2$ et $e \valeur$ alors $e = \lambda (x: \tau) e_2$ pour un certaine variable $x$ une certaine expression $e_2$ telles que $x:\tau_1 \vdash e_2: \tau_2$. 
\paragraph{Théorème (8.4 et 8.6).} EF est /typesafe/.
Faire des inférences de type. Du lambda-calcul simplement typé (cas particulier).
** Types produits, types sommes
** Lambda calcul pur
** Lambda calcul simplement typé 
JavaScript

# Scratch, C, C++, Pascal, Algol, Ada, Logo, Cuda?, early Lisp. Méfi sur C++.
# Snap! (d'où le lambda), Java, Scala, ML, Haskell, OCAML, Clojure, JS, PHP, Lua, Perl etc.
