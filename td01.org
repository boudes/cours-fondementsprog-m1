#+TITLE:   Fondements de la programmation TD 01
#+AUTHOR:   P. Boudes
#+EMAIL:    boudes@univ-paris13.fr

#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE:  fr
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc toc:nil
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+LINK_UP:
#+LINK_HOME:
#+XSLT:
#+STARTUP: latex nolatexpreview entitiesplain
#+LaTeX_CLASS: article
#+LATEX_CLASS_OPTIONS: [11pt, a4paper]
#+LATEX_HEADER: \usepackage[margin=2cm]{geometry}
#+LATEX_HEADER: \usepackage[table]{xcolor}
#+LATEX_HEADER: \usepackage{xspace}
#+LATEX_HEADER: \usepackage{multicol}
#+LATEX_HEADER: \usepackage{bussproofs}
#+LATEX_HEADER: \usepackage{tikz}\usetikzlibrary{arrows,shapes,trees}
#+LATEX_HEADER: \renewcommand{\maketitle}{{\bigskip{\begin{center}\Large\textbf{Fondements de la programmation}\\[0.1cm] Exercices 1 Typage et dynamique\end{center}}}\smallskip}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage[french]{babel}
#+EXCLUDE_TAGS: noexport correction

#+BEGIN_EXPORT latex
\pagestyle{fancyplain}
\fancyhf{}
\lhead{ \fancyplain{}{\raisebox{-1ex}{\includegraphics[scale=0.10]{./img/logoLipnNoir.pdf}} P. Boudes, P. Jacobé de Naurois}}
\rhead{ \fancyplain{}{M1 informatique 2021-2022}}
\rfoot{ \fancyplain{}{\thepage}}
%\rfoot{ }
\newcounter{questioncount}
\setcounter{questioncount}{0}
\newcommand{\question}[1][]{\addtocounter{questioncount}{1}\paragraph{Question \Alph{questioncount}. #1}}
\renewcommand{\subsubsection}[1]{\question[#1.]}
#+END_EXPORT


#+begin_export latex
\EnableBpAbbreviations
\newcommand{\num}{\operatorname{num}}
\newcommand{\str}{\operatorname{str}}
\newcommand{\valeur}{\operatorname{val}}
\newcommand{\letname}{\operatorname{let}}
\newcommand{\letformal}[3]{\letname(#2, #1.#3)}
#+end_export

#+BEGIN_EXPORT latex
\begin{multicols}{2}
#+END_EXPORT

\paragraph{Rappels.} L'essentiel du cours 01 est tiré du chapitre 4 du livre /Practical Foundations of Programming Languages/ de Robert Harper. Nous rappelons les règles de typage du langage. 

Un jugement de typage $\Gamma \vdash e: \tau$ permet d'associer un type $\tau$ à une expression $e$ dans un contexte $\Gamma$. Le contexte décrit un ensemble de variables $\operatorname{dom}(\Gamma)$ et le choix d'un type pour chacune de ces variables. Un jugement de typage est valide s'il peut être dérivé grâce aux règles suivantes :
#+BEGIN_EXPORT latex
  \begin{gather*}
    \AXC{}\RL{id}
  \UIC{$\Gamma, x:\tau\vdash x:\tau$}
  \DP\\[0.5cm]
    \AXC{}\RL{str}
  \UIC{$\Gamma \vdash \str[s]: \str$}
  \DP\quad
    \AXC{}\RL{num}
  \UIC{$\Gamma \vdash \num[n]: \num$}
  \DP\\[0.5cm]
  \AXC{$\Gamma \vdash e_1:\num$}
  \AXC{$\Gamma \vdash e_2:\num$}\RL{+}
  \BIC{$\Gamma \vdash \operatorname{plus}(e_1, e_2):\num$}
  \DP\\[0.5cm]
  \AXC{$\Gamma \vdash e_1:\str$}
  \AXC{$\Gamma \vdash e_2:\str$}\RL{cat}
  \BIC{$\Gamma \vdash \operatorname{cat}(e_1, e_2):\str$}
  \DP\\[0.5cm]
  \AXC{$\Gamma \vdash e_1:\tau_1$}
  \AXC{$\Gamma, x:\tau_1 \vdash e_2:\tau_2$}\RL{let}
  \BIC{$\Gamma \vdash \letformal{x}{e_1}{e_2}:\tau_2$}
  \DP
  \end{gather*}
#+END_EXPORT

\paragraph{Principaux résultats.} Unicité du typage (lemme 4.1). Dans ce système de types, pour toute expression $e$, pour tout contexte $\Gamma$ il y a au plus un type $\tau$ tel que $\Gamma\vdash e:\tau$. Inversion du typage (lemme 4.2). Toute expression du langage correspond à une unique règle de typage ce qui implique que pour qu'une expression soit typable il est nécessaire que ses sous-expressions satisfassent les contraintes données par la règle de typage. Par exemple, pour que $\Gamma \vdash \operatorname{cat}(e_1, e_2): \tau$ il faut que $\tau = \str$, et que $\Gamma \vdash e_i: \str$ ($i = 1, 2$). Affaiblissement (lemme 4.3). On peut toujours ajouter une hypothèse de typage sur une variable fraîche à une dérivation de type (voir exercices). Substitution (lemme 4.4). Étant donné un certain contexte, dans une expression bien typée, on peut toujours remplacer une variable libre par une expression bien typée de même type. Décomposition (lemme 4.5). Réciproquement, on peut toujours remplacer une sous-expression par une variable de même type dans une expression bien typée plus grande. 


\paragraph{Système de transition}
Un système de transition est donné par quatre formes de jugements : $s$ est un état, $s$ est un état final, $s$ est un état initial, $s\mapsto s'$ où $s$ et $s'$ sont des états qui établit que $s$ peut effectuer une transition en $s'$. Un état depuis lequel aucune transition n'est possible est dit bloquant. Par convention nos systèmes de transitions seront toujours tels que tout état final est bloquant. Une système de transition dans lequel dans tout état il existe au plus une transition possible est \emph{déterministe}. Une suite de transitions démarre toujours d'un état initial $s_0$. Elle est \emph{maximale} lorsque le dernier état $s_n$ est bloquant et elle est \emph{complète} si $s_n$ est final. Le jugement $s\downarrow$ signifie qu'il existe une suite de transitions complète démarrant dans l'état $s$.

Une dynamique structurelle d'un langage E est un système de transition dont les états sont les expressions closes (sans variable libre), tous les états sont initiaux, et les états finaux sont les /valeurs/ du langage. On peut avoir plusieurs dynamiques structurelles pour un même langage E (ensemble différents de valeurs, règles de transitions différentes).

Dans notre mini-langage, nous fixons la dynamique structurelle suivante. Seuls les constantes numériques et les littéraux sont des valeurs :

#+begin_export latex
  \begin{gather*}
  \AXC{}\RL{num}
  \UIC{$\num[n] \valeur$}
  \DP\quad
  \AXC{}\RL{str}
  \UIC{$\str[s] \valeur$}
  \DP
  \end{gather*}
#+end_export


Les transitions sont : 

#+begin_export latex
  \begin{gather}
  \AXC{$n_1 + n_2 = n$}\RL{+}
  \UIC{$\operatorname{plus}(\num[n_1], \num[n_2])\mapsto \num[n]$}
  \DP\\[0.5cm]
  \AXC{$e_1\mapsto e'_1$}\RL{+G}
  \UIC{$\operatorname{plus}(e_1, e_2)\mapsto \operatorname{plus}(e'_1, e_2)$}
  \DP\\[0.5cm]
  \AXC{$e_1\valeur \quad e_2 \mapsto e'_2$}\RL{+D}
  \UIC{$\operatorname{plus}(e_1, e_2)\mapsto \operatorname{plus}(e_1, e'_2)$}
  \DP\\[0.5cm]
  \AXC{$e_1 \mapsto e'_1$}\RL{letG}
  \UIC{$\letformal{x}{e_1}{e_2}\mapsto \letformal{x}{e'_1}{e_2}$}
  \left[\DP\right]\\[0.5cm]
  \AXC{$\left[e_1 \valeur\right]$}\RL{letD}
  \UIC{$\letformal{x}{e_1}{e_2}\mapsto [e_1/x]e_2$}
  \DP
  \end{gather}
#+end_export

Les crochets droits signalent les règles (règle 4) et conditions (prémisse de la règle 5) à supprimer pour passer d'une évaluation par valeur à une évaluation par nom.

\paragraph{Type safety} 
Un langage typé équipé d'une dynamique structurelle est dit /type safe/ si (préservation) la dynamique préserve les types et si (progression) il y a toujours au moins une transition possible depuis une expression bien typée qui n'est pas une valeur.

#+BEGIN_EXPORT latex
\end{multicols}
#+END_EXPORT

* Exercices
** Inférence de type
*** Règles
    Donner les règles de typage pour $\operatorname{len}$ et $\operatorname{times}$.

*** Typer
Les expressions suivantes sont-elles typables ? Prouvez-le.
- =let x = "hello" in x^x=
- =let x = 2 in 10 + x=
- ="hello" + 10=
- =let x = "hello" in 10 + x=

*** Typer
Choisir un contexte dans lequel =let x = y^y in len(x) + z= est typable. 
** Preuves des lemmes
*** Affaiblissement
Montrer par induction structurelle sur les règles de typage que si $\Gamma \vdash e': \tau'$ et $x\notin\operatorname{dom}(\Gamma)$ alors pour n'importe quel type $\tau$, $\Gamma, x: \tau\vdash e':\tau'$.
*** Substitution
Montrer que si $\Gamma, x: \tau\vdash e':\tau'$ et $\Gamma\vdash e:\tau$ alors $\Gamma\vdash [e/x]e':\tau'$.
*** Décomposition
Sans vous aider de vos notes de cours, formaliser l'énoncé du lemme 4.5 (réciproque de la substitution). Question subsidiaire : dans un contexte donné, une sous-expression d'une expression bien typée est-elle toujours bien typée ?

** Type safety
*** Blocage
Dans un langage /type safe/ si $e$ est une expression dont l'évaluation est bloquée, sans être une valeur que peut-on dire de $e$ ? 
*** Préservation
Quel(s) méthode(s) de preuve permettrait de démontrer la préservation, c'est à dire que pour toutes expressions $e, e'$ et tout type $\tau$, si $\vdash e:\tau$ et si $e\mapsto e'$ alors $\vdash e':\tau$ ? Faire la preuve pour notre mini-langage.
*** Lemme des formes canoniques
Si $e$ est une valeur et que $\vdash e:\str$ alors $e = \str[s]$ pour un certain $s$. Prouvez-le.
*** Progression
Quelle méthode devrions-nous utiliser pour prouver la progression ? Donner au moins une étape clé de la preuve.


