#+TITLE:   Fondements de la programmation TD 04
#+AUTHOR:   P. Boudes
#+EMAIL:    boudes@univ-paris13.fr

#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE:  fr
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc toc:nil
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+LINK_UP:
#+LINK_HOME:
#+XSLT:
#+STARTUP: latex nolatexpreview entitiesplain
#+LaTeX_CLASS: article
#+LATEX_CLASS_OPTIONS: [11pt, a4paper]
#+LATEX_HEADER: \usepackage[margin=2cm]{geometry}
#+LATEX_HEADER: \usepackage[table]{xcolor}
#+LATEX_HEADER: \usepackage{xspace}
#+LATEX_HEADER: \usepackage{multicol}
#+LATEX_HEADER: \usepackage{bussproofs}
#+LATEX_HEADER: \usepackage{stmaryrd}
#+LATEX_HEADER: \usepackage{tikz}\usetikzlibrary{arrows,shapes,trees}
#+LATEX_HEADER: \renewcommand{\maketitle}{{\bigskip{\begin{center}\Large\textbf{Fondements de la programmation}\\[0.1cm] Exercices 04\end{center}}}\smallskip}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage[french]{babel}
#+EXCLUDE_TAGS: noexport correction

#+BEGIN_EXPORT latex
\pagestyle{fancyplain}
\fancyhf{}
\lhead{ \fancyplain{}{\raisebox{-1ex}{\includegraphics[scale=0.10]{./img/logoLipnNoir.pdf}} P. Boudes, P. Jacobé de Naurois}}
\rhead{ \fancyplain{}{M1 informatique 2021-2022}}
\rfoot{ \fancyplain{}{\thepage}}
%\rfoot{ }
\newcounter{questioncount}
\setcounter{questioncount}{0}
\newcommand{\question}[1][]{\addtocounter{questioncount}{1}\paragraph{Question \Alph{questioncount}. #1}}
\renewcommand{\subsection}[1]{\question[#1.]}
#+END_EXPORT

#+begin_export latex
\overfullrule=5pt
#+end_export

#+begin_export latex
\EnableBpAbbreviations
\newcommand{\num}{\operatorname{num}}
\newcommand{\str}{\operatorname{str}}
\newcommand{\valeur}{\operatorname{val}}
\newcommand{\letname}{\operatorname{let}}
\newcommand{\plusname}{\operatorname{plus}}
\newcommand{\prodname}{\operatorname{prod}}
\newcommand{\sumname}{\operatorname{sum}}
\newcommand{\fixname}{\operatorname{fix}}
\newcommand{\letsurface}[3]{let #1 = #2 in #3}
\newcommand{\letformal}[3]{\letname(#2, #1.#3)}
\newcommand{\apply}{\operatorname{apply}}
\newcommand{\applyformal}[2]{\apply \{#1\}(#2)}
\newcommand{\applysurface}[2]{#1(#2)}
\newcommand{\apformal}[2]{\operatorname{ap}(#1;#2)}
\newcommand{\apsurface}[2]{#1(#2)}
\newcommand{\abssurface}[3]{\ensuremath{\lambda} (#1:#2) #3}
\newcommand{\absformal}[3]{\ensuremath{\lambda}\{#2\}(#1.#3)}
\newcommand{\funname}{\operatorname{fun}}
\newcommand{\funformal}[6]{\funname \{#1; #2\}(#3.#4;#5.#6)}
\newcommand{\funsurface}[6]{fun #5(#3: #1): #2 = #4 in #6}
\newcommand{\error}{\operatorname{error}}
\newcommand{\err}{\operatorname{err}}
\newcommand{\funsub}[3]{\ensuremath{\llbracket#1.#2/#3\rrbracket}}
#+end_export



* Partiel blanc

** Inférences de types
Parmi les expressions suivantes, lesquelles sont typables et avec quel type et lesquelles ne sont pas typables et pourquoi ?
1. =let x = 1 / 0 in 1 + 1=
2. $\lambda\texttt{(x: num) x + (}\lambda\texttt{(y: str) len(y))("x")}$
3. En notant T le type ={Some: num | None: Unit}= :
#+begin_export latex
\begin{alignat*}{5}
 & \lambda\texttt{(f: num }\to\texttt{ num) } \lambda \texttt{(x: T) match x}&&\texttt{ with} \\
  &&&\texttt{| case Some(x1) => Some(f(x1)) as T}\\
  &&&\texttt{| case None(x2) => None(unit) as T}
\end{alignat*}
#+end_export

** Dynamique
Soit =T= le type ={rvb: (num, num, num) | gray: num | name: str}=, réduire à une valeur ou une erreur l'expression suivante en justifiant chaque étape de réduction :
#+begin_src ocaml
let couleurs = 1 in
    let x = ifpos couleurs + (-3) then rvb((0.5, 0.5, 0.5)) as T else gray(0.5) as T
    in match x with
               | case rvb(x1) => div(p1(x1) + p2(x1) + p3(x1) / 3)
               | case gray(x2) => x2
               | case name(x3) => 0   
#+end_src
N'hésitez pas à donner des noms aux sous-expressions.

*** Correction :noexport:
#+begin_src ocaml
let couleurs = 1 in
    let x = ifpos couleurs + (-3) then rvb((0.5, 0.5, 0.5)) as T else gray(0.5) as T
    in match x with
               | case rvb(x1) => div(p1(x1) + p2(x1) + p3(x1) / 3)
               | case gray(x2) => x2
               | case name(x3) => 0   
#+end_src
devient en une étape (letD)
#+begin_src ocaml
let x = ifpos 1 + (-3) then rvb((0.5, 0.5, 0.5)) as T else gray(0.5) as T
in match x with
               | case rvb(x1) => div(p1(x1) + p2(x1) + p3(x1) / 3)
               | case gray(x2) => x2
               | case name(x3) => 0   
#+end_src
qui devient en une étape (letG, ifpos e, +)
#+begin_src ocaml
let x = ifpos -2 then rvb((0.5, 0.5, 0.5)) as T else gray(0.5) as T
in match x with
               | case rvb(x1) => div(p1(x1) + p2(x1) + p3(x1) / 3)
               | case gray(x2) => x2
               | case name(x3) => 0   
#+end_src
qui devient en une étape (letG, ifpos v) 
#+begin_src ocaml
let x = gray(0.5) as T
in match x with
               | case rvb(x1) => div(p1(x1) + p2(x1) + p3(x1) / 3)
               | case gray(x2) => x2
               | case name(x3) => 0   
#+end_src
qui devient en une étape (letD)
#+begin_src ocaml
in match gray(0.5) as T with
               | case rvb(x1) => div(p1(x1) + p2(x1) + p3(x1) / 3)
               | case gray(x2) => x2
               | case name(x3) => 0   
#+end_src
qui devient en une étape (match) =0.5= qui est une valeur.



** Modulo
En utilisant l'expression =letrec= programmer une fonction qui prend en entrée une paire $(a, b)$ de nombres (dont on supposera qu'ils sont des entiers positifs) et retourne la valeur de $a$ modulo $b$ (le reste de la division euclidienne de $a$ par $b$). Calculer $5$ modulo $3$ avec votre fonction, en indiquant les régles de la dynamique utilisées. Vous pouvez omettre de détailler les étapes purement arithmétiques (règles plus, prod, div). Par exemple passer de $1 + 2 + 3 * 4$ à $16$ sans détailler les étapes.

** Langage
Citer un langage de programmation qui ne possède pas les lambdas (les expressions à valeur de fonction) et un langage qui les possède.

** Démonstration
Prouver que si un terme $e$ est une erreur, alors il contient soit un sous-terme $\operatorname{div}(\num[n], \num[0])$ soit un sous-terme $\error$.

*** Correction :noexport:
C ne les possédent pas, ocaml si. 
# Scratch, C, C++, Pascal, Algol, Ada, Logo, Cuda?, early Lisp. Méfi sur C++.
# Snap! (d'où le lambda), Java, Scala, ML, Haskell, OCAML, Clojure, JS, PHP, Lua, Perl etc.
